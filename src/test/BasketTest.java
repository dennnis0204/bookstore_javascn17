package test;

import app.Basket;
import app.Book;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Map;

import static org.assertj.core.api.Assertions.*;

// klasa testowa, testuje klasę Basket
public class BasketTest {
    // posiada pole typu Basket
    // będziemy przypisywać do niego nowe koszyki i na nich testować
    Basket myBasket;

    // metoda cyklu życia, wykonuje się przed każdym testem
    // tworzymy w niej nowy koszyk
    @BeforeEach
    public void createNewBasket() {
        myBasket = new Basket();
    }

    // metoda testowa, testuje metodę returnBasket()
    @Test
    public void testReturnBasket() {
        // tworzymy książkę i dodajemy do koszyka
        myBasket.shoppingList.add(new Book("Title", "author", 10.32));
        // wywołujemy testowaną metodę i zapamietujemy zwrócony obiekt
        ArrayList<Book> actual = myBasket.returnBasket();
        // sprawdzamy czy jest to obiekt typu ArrayList i czy ma jeden element
        assertThat(actual)
                .isInstanceOf(ArrayList.class)
                .hasSize(1);
    }

    // test method for adding one book to the basket
    @Test
    @Tag("TestSuiteBasket")
    public void testAddOneBookToBasket() {
        myBasket.addOneBookToBasket(new Book("1984", "George Orwell", 30));
        myBasket.addOneBookToBasket(new Book("1984", "George Orwell", 30));
        myBasket.addOneBookToBasket(new Book("1984", "George Orwell", 30));

        // check if there are any books with same title in the shoppingList
        int listCounter = (int) myBasket.countBooksBySameTitle("1984");
        assertEquals(1, listCounter);

        // check if there is a book in the bookCounter with quantity < 1
        boolean bookCounterIsValid = true;
        for (Map.Entry<String, Integer> entry : myBasket.bookCounter.entrySet()) {
            if (entry.getValue() < 1) {
                bookCounterIsValid = false;
                break;
            }
        }
        assertTrue(bookCounterIsValid);
    }

    // test method for removing one book from the basket
    @Test
    @Tag("TestSuiteBasket")
    public void testRemoveOneBookFromBasket() {
        // first add one book to the basket
        myBasket.addOneBookToBasket(new Book("1984", "George Orwell", 30));

        // get the number of books by same title in the shoppingList before removing
        int counterBefore = myBasket.bookCounter.getOrDefault("1984", 0);

        // than remove that book from the basket
        myBasket.removeOneBookFromBasket("1984");

        // get the number of books by same title in the shoppingList after removing
        int counterAfter = myBasket.bookCounter.getOrDefault("1984", 0);

        if (counterBefore > 1) {
            // check decrement quantity in the bookCounter
            assertEquals(counterBefore - 1, counterAfter);
        } else {
            // check removing book from the bookCounter
            boolean containsBook = myBasket.bookCounter.containsKey("1984");
            assertFalse(containsBook);
            // check removing book from the shoppingList
            assertNull(myBasket.getBookByTitle("1984"));
        }
    }

    // test method for clearing the basket
    @Test
    @Tag("TestSuiteBasket")
    public void testClearBasket() {
        // first add one book to the basket
        myBasket.addOneBookToBasket(new Book("1984", "George Orwell", 30));

        // clear the basket
        myBasket.clearBasket();

        // test shoppingList size
        assertThat(myBasket.shoppingList.size()).isEqualTo(0);
        // check bookCounter size
        assertThat(myBasket.bookCounter.size()).isEqualTo(0);
    }

    // test method for getNumberOfProductsInBasket
    @Test
    @Tag("TestSuiteBasket")
    public void testGetNumberOfProductsInBasket() {
        myBasket.addOneBookToBasket(new Book("1984", "George Orwell", 30));
        myBasket.addOneBookToBasket(new Book("1984", "George Orwell", 30));
        myBasket.addOneBookToBasket(new Book("1984", "George Orwell", 30));
        myBasket.addOneBookToBasket(new Book("Pride and Prejudice", "Jane Austen", 28));
        int numberOfProducts = myBasket.getNumberOfProductsInBasket();

        // check if number of products in the basket not negative
        assertTrue(numberOfProducts >= 0);

        // check if number of books in the shoppingList not greater than
        // number of products in the basket
        assertTrue(numberOfProducts >= myBasket.shoppingList.size());
    }

    // test method for cost of products in the basket
    @Test
    @Tag("TestSuiteBasket")
    public void testGetCostOfProductsInBasket() {
        myBasket.addOneBookToBasket(new Book("1984", "George Orwell", 30));
        myBasket.addOneBookToBasket(new Book("1984", "George Orwell", 30));
        myBasket.addOneBookToBasket(new Book("Pride and Prejudice", "Jane Austen", 28));
        myBasket.addOneBookToBasket(new Book("Pride and Prejudice", "Jane Austen", 28));

        double costOfProducts = myBasket.getCostOfProductsInBasket();

        // check if cost of products in the basket not negative
        assertTrue(costOfProducts > 0);
    }
}
