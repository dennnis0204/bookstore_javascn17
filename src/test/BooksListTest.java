package test;

import app.Book;
import app.BooksList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

// klasa testowa, testuje klasę BooksList
public class BooksListTest {
    // posiada pole typu BooksList
    // będziemy przypisywać do niego nowe listy książek i na nich testować
    BooksList bList;

    // metoda cyklu życia, wykonuje się przed każdym testem
    // tworzymy w niej nową listę książek
    @BeforeEach
    public void createList(){
        bList = new BooksList();
    }

    // metoda testowa, testuje metodę addBooks()
    @Test
    public void testAddBook(){
        // tworzymy książkę
        Book book = new Book("Title", "Author", 20.5);
        // zapamiętujemy liczbę książek na liście (pusta, 0)
        int elCountBefore = bList.books.size();
        // dodajemy książkę na listę
        bList.addBook(book);
        // sprawdzamy czy po powyższej operacji lista ma o 1 więcej książek (w tym przypadku 1)
        assertThat(bList.books.size()).isEqualTo(elCountBefore + 1);
    }

    /*========================================================================================*/
    // my own tests

    /**
     *  1. test for list size before and after removing book from the list
     *  2. test for existing this book in list after removing
     */
    @Test
    @Tag("TestSuiteList")
    @DisplayName("MyFirstOwnTest \uD83D\uDE03")
    public void testRemoveBook() {
        // add some books to list
        bList.addBook(new Book("1984", "George Orwell", 30));
        bList.addBook(new Book("Pride and Prejudice", "Jane Austen", 28));

        // quantity of books before removing from the list
        int elCounterBefore = bList.books.size();

        // find the book with some title
        Book targetBook = bList.findBookByTitle("1984");

        // remove the book from the list
        bList.removeBook(targetBook);

        // quantity of books after removing from the list
        int elCounterAfter = bList.books.size();

        // check the size of the list after removing
        assertThat(elCounterAfter)
                .isEqualTo(elCounterBefore - 1);

        // check whether the same book is in the list after removing
        assertThat(bList.findBookByTitle("1984"))
                .isNull();
    }

    /**
     *  1. negative price value test
     *  2. checking the difference between the old and new price values
     */

    @Test
    @DisplayName("PriceChangeTest")
    public void testChangePrice() {

        // add some books to list
        bList.addBook(new Book("1984", "George Orwell", 30));
        bList.addBook(new Book("Pride and Prejudice", "Jane Austen", 28));

        // find the book with some title
        Book targetBook = bList.findBookByTitle("1984");

        // save old price to variable
        double oldPrice = targetBook.getPrice();

        // change price
        targetBook.changePrice(35);

        // checking for negative price value
        assertTrue(targetBook.getPrice() > 0);

        // checking the difference between the old and new price values
        assertNotEquals(targetBook.getPrice(), oldPrice);
    }
}
