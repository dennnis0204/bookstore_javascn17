package app;

import java.util.ArrayList;

// klasa BooksList
public class BooksList {
    // posiada pole, będące listą dostępnych książek
    public ArrayList<Book> books = new ArrayList<>();

    // metoda dodająca książkę do listy
    public void addBook(Book book) {
        books.add(book);
    }

    /*=================================================*/

    // remove the book from the list
    public void removeBook(Book book) {
        books.remove(book);
    }

    // find the book from the list by title
    public Book findBookByTitle(String title) {
        return books
                .stream()
                .filter(book -> title.equals(book.getTitle()))
                .findFirst()
                .orElse(null);
    }
}
