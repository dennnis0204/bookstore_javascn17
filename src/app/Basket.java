package app;

import java.util.ArrayList;
import java.util.HashMap;

// klasa Basket
public class Basket {
    // posiada pole, będące listą książek w koszyku
    public ArrayList<Book> shoppingList = new ArrayList<Book>();

    // counter for the number of the same books in the cart
    public HashMap<String, Integer> bookCounter = new HashMap<>();

    // metoda zwracająca listę książek w koszyku
    public ArrayList<Book> returnBasket() {
        return shoppingList;
    }

    // get first book from the basket by title
    public Book getBookByTitle(String title) {
        return shoppingList
                .stream()
                .filter(book -> title.equals(book.getTitle()))
                .findFirst()
                .orElse(null);
    }

    // count books by same title in the basket
    public long countBooksBySameTitle(String title) {
        return shoppingList
                .stream()
                .filter(book -> title.equals(book.getTitle()))
                .count();
    }


    // add one book to the basket
    public void addOneBookToBasket(Book book) {
        if (getBookByTitle(book.getTitle()) != null) {
            // increment quantity by one in the bookCounter
            Integer newQuantity = bookCounter.get(book.getTitle()) + 1;
            bookCounter.put(book.getTitle(), newQuantity);
        } else {
            // add book to the shoppingList
            shoppingList.add(book);
            // add book to the bookCounter
            bookCounter.put(book.getTitle(), 1);
        }
    }

    // remove one book from the basket
    public void removeOneBookFromBasket(String title) {
        int counter = bookCounter.getOrDefault("1984", 0);
        if (counter > 1) {
            // decrement quantity by one in the bookCounter
            bookCounter.put(title, counter - 1);
        } else {
            // remove book from the shoppingList
            bookCounter.remove(title);
            // remove book from the bookCounter
            shoppingList.remove(getBookByTitle(title));
        }
    }

    // clear the basket
    public void clearBasket() {
        bookCounter.clear();
        shoppingList.clear();
    }

    // get number of products in the basket
    public int getNumberOfProductsInBasket() {
        int numberOfProductsInBasket = 0;
        for (HashMap.Entry<String, Integer> entry : bookCounter.entrySet()) {
            numberOfProductsInBasket += entry.getValue();
        }
        return numberOfProductsInBasket;
    }

    // get cost of products in the basket
    public double getCostOfProductsInBasket() {
        String bookTitle;
        double costOfProducts = 0;
        for (HashMap.Entry<String, Integer> entry : bookCounter.entrySet()) {
            bookTitle = entry.getKey();
            costOfProducts += getBookByTitle(bookTitle).getPrice() * entry.getValue();
        }
        return costOfProducts;
    }
}
